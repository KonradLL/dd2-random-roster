import styled from "styled-components";
import "./App.css";
import RollRoller from "./components/RosterRoller";
// import RollSwapper from "./components/RosterSwapper";

const ContentWrapper = styled.div`
	width: 60%;
	height: 95%;
	margin: auto;
	padding-top: 5%;
`;

const Header = styled.h1`
	margin: auto;
	height: 10%;

	text-align: center;
	font-size: 80px;
	color: #75613c;
`;

function App() {
	return (
		<ContentWrapper>
			<Header>Roll Your Roster</Header>
			<RollRoller />
		</ContentWrapper>
	);
}

export default App;
