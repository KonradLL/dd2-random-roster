import { useEffect, useRef, useState } from "react";
import Slider from "react-slick";
import characters from "../data/roster";
import empty from "../data/characters/Empty_portrait_roster.png";
import styled from "styled-components";

const RollWrapper = styled.div`
	height: 80%;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
`;

const GridRow = styled.div`
	margin-top: 10rem;
	width: 60%;
	max-height: 126px;
	display: flex;
	flex-direction: row;
	justify-content: space-around;
	overflow: hidden;
`;

const RollButton = styled.button`
	text-align: center;
	margin-top: 2rem;
	width: 14rem;
	height: 4rem;

	color: white;
	font-width: 5px;
	font-size: 25px;
	background-color: #111;
	border: 3px solid #222;

	&:active {
		border-color: #ca974e;
		color: #ca974e;
	}
`;

const BigText = styled.p`
	color: white;
	font-size: 20px;
`;

const FAST_SLIDER_SPEED = 150;
const MEDIUM_SLIDER_SPEED = 500;
const SLOW_SLIDER_SPEED = 900;
const defaultSettings = {
	accessibility: false,
	arrows: false,
	dots: false,
	// infinite: true,
	pauseOnHover: false,
	sliderToShow: 1,
	vertical: true,
	verticalSwiping: false,
	swipe: false,
	autoplay: true,
	autoplaySpeed: 150,
	ease: "ease-in",
	// rtl: true,
};

type Slick = {
	ref: React.RefObject<Slider>;
	index: number;
	speed: number;
	roster: string[];
};

function getNewRoster() {
	return [...characters].sort(() => Math.random() - 0.5);
}

function RollRoller() {
	const [roster, setRoster] = useState([empty]);
	const rosterRef = useRef(roster);
	rosterRef.current = roster;

	const defaultSlick = {
		index: 0,
		speed: FAST_SLIDER_SPEED,
		roster: roster,
	};

	const [slider1, setSlider1] = useState<Slick>({
		...defaultSlick,
		ref: useRef<Slider>(null),
	});
	const [slider2, setSlider2] = useState<Slick>({
		...defaultSlick,
		ref: useRef<Slider>(null),
	});
	const [slider3, setSlider3] = useState<Slick>({
		...defaultSlick,
		ref: useRef<Slider>(null),
	});
	const [slider4, setSlider4] = useState<Slick>({
		...defaultSlick,
		ref: useRef<Slider>(null),
	});

	const slider1Ref = useRef(slider1);
	slider1Ref.current = slider1;
	const slider2Ref = useRef(slider2);
	slider2Ref.current = slider2;
	const slider3Ref = useRef(slider3);
	slider3Ref.current = slider3;
	const slider4Ref = useRef(slider4);
	slider4Ref.current = slider4;

	const sliders = [slider1, slider2, slider3, slider4];
	const sliderSetters = [setSlider1, setSlider2, setSlider3, setSlider4];

	useEffect(() => {
		sliders.forEach((slider) => slider.ref.current?.slickPause());
		// eslint-disable-next-line
	}, []);

	function startCarousel() {
		sliders.forEach((slider, index) => {
			setTimeout(() => {
				slider.ref.current?.slickPlay();
			}, index * 200);
		});
	}

	function stopCarousel() {
		// ----- First -----
		setTimeout(() => {
			setSlider1({ ...slider1Ref.current, speed: MEDIUM_SLIDER_SPEED });
			setTimeout(() => {
				slider1Ref.current.ref.current?.slickPause();
				setSlider1({ ...slider1Ref.current, roster: rosterRef.current.splice(slider1Ref.current.index, 1) });
				setRoster(rosterRef.current);
			}, 2000);
		}, 800);

		// ----- Second -----
		setTimeout(() => {
			setSlider2({ ...slider2Ref.current, speed: MEDIUM_SLIDER_SPEED });
			setTimeout(() => {
				slider2Ref.current.ref.current?.slickPause();
				setTimeout(() => {
					setSlider2({ ...slider2Ref.current, roster: rosterRef.current.splice(slider2Ref.current.index, 1) });
					setRoster(rosterRef.current);
				}, 500);
			}, 1800);
		}, 3500);

		// ----- Third -----
		setTimeout(() => {
			setSlider3({ ...slider3Ref.current, speed: MEDIUM_SLIDER_SPEED });
			setTimeout(() => {
				setSlider3({ ...slider3Ref.current, speed: SLOW_SLIDER_SPEED });
				setTimeout(() => {
					slider3Ref.current.ref.current?.slickPause();
					setTimeout(() => {
						setSlider3({ ...slider3Ref.current, roster: rosterRef.current.splice(slider3Ref.current.index, 1) });
						setRoster(rosterRef.current);
					}, 500);
				}, 2000);
			}, 2000);
		}, 7000);

		// ----- Forth -----
		setTimeout(() => {
			setSlider4({ ...slider4Ref.current, speed: MEDIUM_SLIDER_SPEED });
			setTimeout(() => {
				setSlider4({ ...slider4Ref.current, speed: SLOW_SLIDER_SPEED });
				setTimeout(() => {
					slider4Ref.current.ref.current?.slickPause();
					setTimeout(() => {
						setSlider4({ ...slider4Ref.current, roster: rosterRef.current.splice(slider4Ref.current.index, 1) });
						setRoster(rosterRef.current);
						console.log([...rosterRef.current]);
					}, 500);
				}, 2000);
			}, 2000);
		}, 12500);
	}

	function resetCarousel() {
		const newRoster = getNewRoster();
		setRoster(newRoster);

		sliders.forEach((slider, index) =>
			sliderSetters[index]({
				...slider,
				index: 0,
				roster: newRoster,
				speed: FAST_SLIDER_SPEED,
			})
		);
	}

	function roll() {
		resetCarousel();
		console.log([...roster]);

		startCarousel();
		setTimeout(stopCarousel, 700);
	}

	return (
		<RollWrapper>
			<div style={{ display: "none" }}>
				<BigText>{slider1.index}</BigText>
				<BigText>{slider2.index}</BigText>
				<p style={{ color: "white" }}>{roster}</p>
			</div>
			<GridRow>
				{sliders.map((slider, index) => (
					<Slider
						key={index}
						{...defaultSettings}
						ref={slider.ref}
						speed={slider.speed}
						autoplaySpeed={slider.speed - 20}
						beforeChange={(curr, next) => sliderSetters[index]({ ...slider, index: next })}
						initialSlide={index * 2}
					>
						{slider.roster.map((src, index) => (
							<img key={index} width="120" height="120" src={src} alt={index.toString()} />
						))}
					</Slider>
				))}
			</GridRow>
			<RollButton onClick={() => roll()}>Roll</RollButton>
		</RollWrapper>
	);
}

export default RollRoller;
