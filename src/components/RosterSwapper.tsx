import { useState } from "react";
import characters from "../data/roster";
import empty from "../data/characters/Empty_portrait_roster.png";
import styled from "styled-components";

const RollWrapper = styled.div`
	height: 80%;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
`;

const GridRow = styled.div`
	margin-top: 10rem;
	width: 60%;
	display: flex;
	flex-direction: row;
	justify-content: space-around;
`;

const RollButton = styled.button`
	text-align: center;
	margin-top: 2rem;
	width: 14rem;
	height: 4rem;

	color: white;
	font-width: 5px;
	font-size: 25px;
	background-color: #111;
	border: 3px solid #222;

	&:active {
		border-color: #ca974e;
		color: #ca974e;
	}
`;

function getRoster() {
	return [...characters].sort(() => Math.random() - 0.5);
}

function RollSwapper() {
	const [roster, setRoster] = useState(getRoster());

	const [randChar1, setRandChar1] = useState(empty);
	const [randChar2, setRandChar2] = useState(empty);
	const [randChar3, setRandChar3] = useState(empty);
	const [randChar4, setRandChar4] = useState(empty);

	function roll() {
		setRoster(getRoster());

		const interval1 = setInterval(() => {
			setRandChar1(roster[Math.floor(Math.random() * roster.length)]);
		}, 100);

		setTimeout(() => {
			setRandChar1(roster.pop()!);
			clearInterval(interval1);
		}, 2000);

		const interval2 = setInterval(() => {
			setRandChar2(roster[Math.floor(Math.random() * roster.length)]);
			console.log(randChar2);
		}, 100);

		setTimeout(() => {
			setRandChar2(roster.pop()!);
			clearInterval(interval2);
		}, 4000);

		const interval3 = setInterval(() => {
			setRandChar3(roster[Math.floor(Math.random() * roster.length)]);
		}, 100);

		setTimeout(() => {
			setRandChar3(roster.pop()!);
			clearInterval(interval3);
		}, 6000);

		const interval4 = setInterval(() => {
			setRandChar4(roster[Math.floor(Math.random() * roster.length)]);
		}, 100);

		setTimeout(() => {
			setRandChar4(roster.pop()!);
			clearInterval(interval4);
		}, 8000);
	}

	return (
		<RollWrapper>
			<GridRow>
				<img width="120" height="120" src={randChar1} alt="first hero" />
				<img width="120" height="120" src={randChar2} alt="second hero" />
				<img width="120" height="120" src={randChar3} alt="third hero" />
				<img width="120" height="120" src={randChar4} alt="fourth hero" />
			</GridRow>
			<RollButton onClick={() => roll()}>Roll</RollButton>
		</RollWrapper>
	);
}

export default RollSwapper;
