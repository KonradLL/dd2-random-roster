
import robber from './characters/Grave_robber_portrait_roster.png'
import hellion from './characters/Hellion_portrait_roster.png'
import runaway from './characters/Runaway_portrait_roster.png'
import highwayman from './characters/Highwayman_portrait_roster.png'
import jester from './characters/Jester_portrait_roster.png'
import leper from './characters/Leper_portrait_roster.png'
import occultist from './characters/Occultist_portrait_roster.png'
import plague from './characters/Plague_doctor_portrait_roster.png'
import man from './characters/Man-at-arms_portrait_roster.png'

const roster = [
  robber,
  hellion,
  runaway,
  highwayman,
  jester,
  leper,
  occultist,
  plague,
  man,
]

export default roster